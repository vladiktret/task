var r1 = document.getElementById("r1");
var r2 = document.getElementById("r2");

r1.onchange = uncoverOnCheck;
r2.onchange = uncoverOnCheck;

var form = document.getElementsByTagName("form").item(0);

form.sub.onclick = checkAndSubmit;
form.clearbut.onclick = clear;

function uncoverOnCheck(){
    var form1 = document.getElementsByClassName("form1").item(0);
    var form2 = document.getElementsByClassName("form2").item(0);
    if(r1.checked && !r2.checked){
        form1.classList.remove("hidden");
        form2.classList.add("hidden")
    } else if(!r1.checked && r2.checked){
        form1.classList.add("hidden");
        form2.classList.remove("hidden");
    } else{
        form1.classList.add("hidden");
        form2.classList.add("hidden");
    }
}

function checkAndSubmit(){
    var name = document.getElementsByName("name").item(0);
    var family = document.getElementsByName("family").item(0);
    var errorname = document.getElementById("errorname");
    var errorfamily = document.getElementById("errorfamily");
    if(name.value == ''){
        errorname.classList.remove("hidden");
    } else {
        errorname.classList.add("hidden");
    }
    if(family.value == ''){
        errorfamily.classList.remove("hidden");
    } else {
        errorfamily.classList.add("hidden");
    }
    if(errorname.classList.contains("hidden") || errorfamily.classList.contains("hidden")){
        //return false;
    } else {
        //form.submit();
    }
}

function clear(){
    r1.checked = false;
    r2.checked = false;
    form.age.value = '';
    document.getElementsByName("name").item(0).value = '';
    document.getElementsByName("family").item(0).value = '';
    uncoverOnCheck();
}